"""Crear un programa que permita:
Crear los puntos A(2, 3), B(5,5), C(-3, -1) y D(0,0) e imprímalos por pantalla.
Consultar a que cuadrante pertenecen el punto A, C y D.
Consultar la distancia entre los puntos 'A y B' y 'B y A'.
Determinar cual de los 3 puntos A, B o C, se encuentra más lejos del origen, punto (0,0).
Crea un rectángulo utilizando los puntos A y B.
Consultar la base, altura y área del rectángulo."""

import math


class Punto:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("{} Se halla en el primer cuadrante".format(self))
        elif self.x < 0 and self.y > 0:
            print("{} Se halla en el segundo cuadrante".format(self))
        elif self.x < 0 and self.y < 0:
            print("{} Se halla en el tercer cuadrante".format(self))
        elif self.x > 0 and self.y < 0:
            print("{} Se halla en el cuarto cuadrante".format(self))
        elif self.x != 0 and self.y == 0:
            print("{} se sitúa sobre el eje X".format(self))
        elif self.x == 0 and self.y != 0:
            print("{} se sitúa sobre el eje Y".format(self))
        else:
            print("{} se encuentra en el origen".format(self))

    def vector(self, p):
        print("El vector entre {} y {} es ({}, {})".format(
            self, p, p.x - self.x, p.y - self.y))

    def distancia(self, p):
        d = math.sqrt((p.x - self.x) ** 2 + (p.y - self.y) ** 2)
        print("La distancia entre los puntos {} y {} es {}".format(self, p, d))

    def __str__(self):
        return "({}, {})".format(self.x, self.y)


class Rectangulo:
    def __init__(self, punt_inicial=Punto(0, 0), punt_final=Punto(0, 0)):
        self.punt_inicial = punt_inicial
        self.punt_final = punt_final

        self.ba = abs(self.punt_final.x - self.punt_inicial.x)
        self.h = abs(self.punt_final.y - self.punt_inicial.y)
        self.A = self.ba * self.h

    def base(self):
        print("Base del retangulo es {}".format(self.ba))

    def altura(self):
        print("Altura del retangulo es {}".format(self.h))

    def area(self):
        print("Area del retangulo es {}".format(self.A))


A = Punto(2, 3)
B = Punto(5, 5)
C = Punto(-3, -1)
D = Punto(0, 0)
print("El punto A es:", A)
print("El punto B es:", B)
print("El punto C es:", C)
print("El punto D es:", D)

A.cuadrante()
C.cuadrante()
D.cuadrante()


A.distancia(B)
B.distancia(A)

print("El punto que esta mas alejado del origen es:")

A.distancia(D)
B.distancia(D)
C.distancia(D)

R = Rectangulo(A, B)
R.base()
R.altura()
R.area()
